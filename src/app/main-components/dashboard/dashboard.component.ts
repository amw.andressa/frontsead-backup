import { Component, Input, OnInit } from '@angular/core';
import {Router} from '@angular/router'

interface Cards {
  icon_card:       string;
  titulo_card:     string;
  descricao_card:  string;
  btn_atalho_card: string;
  link_card:       string;
}

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  @Input() cards: Array<Cards> = [];
  
  constructor(private router: Router) { 

  }

  ngOnInit(): void {}

  

}
