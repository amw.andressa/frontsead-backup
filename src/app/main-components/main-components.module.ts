import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TopMenuComponent } from './top-menu/top-menu.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatSidenavModule} from '@angular/material/sidenav';
import { MatButtonModule } from '@angular/material/button';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SidenavComponent } from './sidenav/sidenav.component';

import {MatCardModule} from '@angular/material/card';
import {MatDividerModule} from '@angular/material/divider';
import {MatListModule} from '@angular/material/list';
import { DashboardComponent } from './dashboard/dashboard.component';
import {MatBadgeModule} from '@angular/material/badge';
import { DashboardsRoutingModule } from '../dashboards/dashboards-routing.module';


@NgModule({
  declarations: [
    TopMenuComponent,
    SidenavComponent,
    DashboardComponent
  ],
  imports: [
    CommonModule,
    MatToolbarModule,
    MatIconModule,
    MatMenuModule,
    MatSidenavModule,
    MatButtonModule,

    FlexLayoutModule,
    MatCardModule,
    MatDividerModule,
    MatListModule,
    MatBadgeModule,
    DashboardsRoutingModule

  ],
  exports:[
    TopMenuComponent,
    SidenavComponent,
    DashboardComponent

  ]
})
export class MainComponentsModule { }
