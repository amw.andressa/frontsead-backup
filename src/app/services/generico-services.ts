import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

export class GenericService<T> {

  constructor(protected httpClient: HttpClient, private API_URL: string) { }

  listAll(): Observable<T[]> {
    return this.httpClient.get<T[]>(this.API_URL)
          // .pipe(
          //   first(),
          //   // tap(p => console.log(p))
          // )
  }


  getOne(id: number): Observable<T> {
    return this.httpClient.get<T>(this.API_URL + '?q=id::' + id.toString())
  }

  insert(p: T) {
    return this.httpClient.post(this.API_URL, p);
  }

  update(id: number, p: T): Observable<T> {
    return this.httpClient.put<T>(this.API_URL + '/' + id, JSON.stringify(p))
  }

  delete(id: number) {
    return this.httpClient.delete(this.API_URL + "/" + id.toString());
  }

   getByMatricula(matricula: string): Observable<T[]> {
     return this.httpClient.get<T[]>(this.API_URL + '/matricula/' + matricula.toString())
   }

  getOneByCpf(cpf: string): Observable<T> {
    return this.httpClient.get<T>(this.API_URL + 'cpf?q=cpf::' + cpf.toString())
  }

  getOneByMatricula(matricula: string): Observable<T> {
    return this.httpClient.get<T>(this.API_URL + '?q=matricula::' + matricula.toString())
  }

  getByName(name: string): Observable<T[]> {
    return this.httpClient.get<T[]>(this.API_URL + '?q=nome:>' + name)
  }

  getByNameServidor(name: string): Observable<T[]> {
    return this.httpClient.get<T[]>(this.API_URL + '/nome/' + name.toUpperCase())
  }

  getOneByCpfServidor(cpf: string): Observable<T> {
    return this.httpClient.get<T>(this.API_URL + '/cpf/' + cpf.toString())
  }

}