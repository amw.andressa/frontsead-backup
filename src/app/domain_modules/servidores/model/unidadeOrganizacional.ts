export interface unidadeOrganizacional {
    codIbgeCnae: string,
    codigoLegado: string,
    dataExtincao: string,
    inicioOperacao: string,
    pessoaJuridica: PessoaJuridica[]
}

export interface PessoaJuridica{
    classificacaoTributaria: string,
    cnpj: string,
    email: string,
    nome: string,
    razaoSocial: string,
    endereco: Endereco[]
}

export interface Endereco{
    bairro: string,
    cep: string,
    cidade: string,
    complemento: string,
    logradouro: string,
    numero: string,
    pais: string,
    tipoLogradouro: string,
    unidadeFederativa: string
}