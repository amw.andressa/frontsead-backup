export interface Vinculo {
    exercicio: string,
    posse: string,
    nomeacao: string,
    regimeJuridico: any,
    situacao: string,
    tipoVinculo: any,
    nivelCargo: any,
    funcao: any,
    unidadeOrganizacional: any,
    setor: any
}