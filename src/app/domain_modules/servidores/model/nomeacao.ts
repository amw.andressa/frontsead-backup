export interface Nomeacao {
    exercicio: string, 
    posse: string,
    nomeacao: string,
    descontaIRPF: boolean,
    situacao: any,
    regimeJuridico: any,
    tipoVinculo: any,
    servidor: number,
    nivelCargo: number,
    funcao: number,
    setor: number,
    unidadeOrganizacional: number,
    tipoEvento: string,
    tipoNomeacao:string,
    processoAdministrativo: string,
    vigencia: string,
    descricao: string
}

