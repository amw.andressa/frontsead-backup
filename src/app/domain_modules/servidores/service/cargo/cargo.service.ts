import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GenericService } from 'src/app/services/generico-services';
import { Cargo } from '../../model/cargo';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CargoService extends GenericService<Cargo>{

  constructor(protected http: HttpClient) { 

    super(http, environment.apiUrl + 'cargos');  
  }
}
