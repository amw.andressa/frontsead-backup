import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GenericService } from 'src/app/services/generico-services';
import { Servidor } from '../../model/servidor';

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ServidorService extends GenericService<Servidor>{


  constructor(protected http: HttpClient) {
    
    super(http, environment.apiUrl + 'servidores');  
  }
}





