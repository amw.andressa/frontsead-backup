import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GenericService } from 'src/app/services/generico-services';
import { Nomeacao } from '../../model/nomeacao';

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class NomeacaoService extends GenericService<Nomeacao>{

  constructor(protected http: HttpClient) { 

    super(http, environment.apiUrl + 'nomeacoes');  
  }
}
