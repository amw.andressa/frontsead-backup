import { TestBed } from '@angular/core/testing';

import { VinculosService } from './vinculos.service';

describe('VinculosService', () => {
  let service: VinculosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VinculosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
