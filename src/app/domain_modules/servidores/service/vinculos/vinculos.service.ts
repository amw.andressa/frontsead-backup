import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Servidor } from '../../model/servidor';
import { GenericService } from 'src/app/services/generico-services';
import { environment } from 'src/environments/environment';
import { Vinculo } from '../../model/vinculo';
@Injectable({
  providedIn: 'root'
})
export class VinculosService extends GenericService<Vinculo>{

  constructor(protected http: HttpClient) {
    
    super(http, environment.apiUrl + 'vinculos');  
  }
}