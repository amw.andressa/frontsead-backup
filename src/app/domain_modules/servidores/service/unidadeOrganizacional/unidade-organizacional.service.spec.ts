import { TestBed } from '@angular/core/testing';

import { UnidadeOrganizacionalService } from './unidade-organizacional.service';

describe('UnidadeOrganizacionalService', () => {
  let service: UnidadeOrganizacionalService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UnidadeOrganizacionalService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
