import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GenericService } from 'src/app/services/generico-services';
import { unidadeOrganizacional } from '../../model/unidadeOrganizacional';

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UnidadeOrganizacionalService extends GenericService<unidadeOrganizacional>{

  constructor(protected http: HttpClient) { 

    super(http, environment.apiUrl +'uo');  
  }
}
