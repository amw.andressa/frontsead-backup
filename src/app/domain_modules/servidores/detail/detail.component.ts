import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Servidor } from '../model/servidor';
import { Vinculo } from '../model/vinculo';
import { ServidorService } from '../service/servidor/servidor.service';
import { VinculosService } from '../service/vinculos/vinculos.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
  
  servidor$!: Observable<Servidor>
  servidorObj!: any;

  vinculo$!: Observable<Vinculo>
  vincunlo_array!: any;

  displayedColumns = ['posse', 'regimeJuridico', 'tipoVinculo', 'nivelCargo.cargoUO.cargo.denominacao', 'funcao.denominacao', 'situacao', 'detalhar']

  form: FormGroup;

  servidor: Servidor = {
    id: '',
    matricula: '',
    pessoaFisica: ''  
  }
  
  constructor(
    private formBuilder: FormBuilder,
    private servidorService: ServidorService,
    private vinculoService: VinculosService,
    private route: ActivatedRoute,

    private router: Router,
  ) {

    this.form = this.formBuilder.group({
      id: [null],
      matricula: [null],
      cpf: [null],
      sexo: [null],
      nascimento: [null],
      nome: [null],
      nomeMae: [null],
      nomePai: [null],
    });
   }


   ngOnInit(): void {
      this.getOne();   
    };
    
    
    getOne(){
    const id = Number(this.route.snapshot.paramMap.get('id')); 
    
    if (id){
      this.servidor$ = this.servidorService.getOne(id);
    
      this.servidorService.getOne(id).subscribe(s => { 
        this.servidorObj = s 
        this.loadForm(this.servidorObj.content[0])
      });
    }
  }


  enviarDetalhesServidor() {

    this.router.navigate(['servidores/nomeacao/' + this.route.snapshot.paramMap.get('id')], {})
  }

  loadForm(servidor: Servidor) {
    this.form = this.formBuilder.group({
      matricula: [{value: servidor.matricula,  disabled: true}],
      cpf: [{value: servidor.pessoaFisica.cpf,  disabled: true}],
      sexo: [{value: servidor.pessoaFisica.genero, disabled: true}],
      nascimento: [{value: this.ToDate(servidor.pessoaFisica.nascimento), disabled: true}],
      nome: [{value: servidor.pessoaFisica.nome, disabled: true}], 
      nomeMae: [{value: servidor.pessoaFisica.nomeMae, disabled: true}],
      nomePai: [{value: servidor.pessoaFisica.nomePai, disabled: true}],
    })
    this.vinculoService.getByMatricula(servidor.matricula).subscribe(p => {this.vincunlo_array = Object.values(p)[0], console.log(this.vincunlo_array)})
  }

  ToDate(date:any){
    return new Date(date).toLocaleDateString()
  }

}
