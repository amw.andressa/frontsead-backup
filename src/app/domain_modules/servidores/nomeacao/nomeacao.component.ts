import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { catchError, Observable, of, tap } from 'rxjs';
import { Cargo } from '../model/cargo';
import { Servidor } from '../model/servidor';
import { CargoService } from '../service/cargo/cargo.service';
import { NomeacaoService } from '../service/nomeacao/nomeacao.service';
import { ServidorService } from '../service/servidor/servidor.service';
import { UnidadeOrganizacionalService } from '../service/unidadeOrganizacional/unidade-organizacional.service';


@Component({
  selector: 'app-nomeacao',
  templateUrl: './nomeacao.component.html',
  styleUrls: ['./nomeacao.component.scss']
})

export class NomeacaoComponent implements OnInit {

  servidor$!: Observable<Servidor>
  servidorObj!: any;
  form: FormGroup;
  servidor: Servidor = {
    id: '',
    matricula: '',
    pessoaFisica: ''  
  }

  unidadesOrganizacionais_array: any;
  cargos_array: any;

  form_nomeacao: FormGroup;
  
  constructor(
    private formBuilder: FormBuilder,
    private servidorService: ServidorService,
    private nomeacaoService: NomeacaoService,
    private cargoService: CargoService,
    private unidadeOrganizacionalService: UnidadeOrganizacionalService,
    private route: ActivatedRoute,
    private router: Router,
    private snackBar: MatSnackBar,
  ) {

    this.form = this.formBuilder.group({
      id: [null],
      matricula: [null],
      cpf: [null],
      sexo: [null],
      nascimento: [null],
      nome: [null],
      nomeMae: [null],
      nomePai: [null],
    });

    this.form_nomeacao = this.formBuilder.group({
      exercicio:[null],
      posse: [null],
      nomeacao: [null],
      descontaIRPF: [null],
      situacao: [null],
      regimeJuridico: [null],
      tipoVinculo: [null],
      servidor: [null],
      nivelCargo: [null],
      funcao: [null],
      setor: [null],
      unidadeOrganizacional: [null],
      tipoEvento: [null],
      processoAdministrativo: [null],
      });

    this.cargoService.listAll().subscribe(p => {this.cargos_array = Object.values(p)[0], console.log(Object.entries(this.cargos_array))})
    this.unidadeOrganizacionalService.listAll().subscribe(p => {this.unidadesOrganizacionais_array = Object.values(p)[0], console.log(Object.entries(this.unidadesOrganizacionais_array))})

   }

   ngOnInit(): void {
      this.getOne();
    };
    
    onSubmit(){
      const id = Number(this.route.snapshot.paramMap.get('id'));
      this.form_nomeacao.value.servidor = id
      this.form_nomeacao.value.situacao = 0 // Situação Ativo
      this.form_nomeacao.value.tipoEvento = 0 // Evento NOMEACAO_DE_EFETIVO_CIVIL
      this.form_nomeacao.value.descricao = "teste" //Levantar o que deve ser passado nesse campo
      
      this.nomeacaoService.insert(this.form_nomeacao.value).subscribe({
        next: (v) => this.onSucess(),
        error: (e) => this.snackBar.open(e, "", {duration:1000}),
        complete: () => console.info('complete')
        })
    }

    onSucess(){
      this.router.navigate(['servidores/detalhar/' + this.route.snapshot.paramMap.get('id')], {});
      this.snackBar.open("Cadastro realizado com sucesso", "", {duration:2500})
    }
    
    getOne(){
    const id = Number(this.route.snapshot.paramMap.get('id')); 
    
    if (id){
      this.servidor$ = this.servidorService.getOne(id);
    
      this.servidorService.getOne(id).subscribe(s => { 
        this.servidorObj = s 
        this.loadForm(this.servidorObj.content[0])
      });
    }
  }

  onError(msg:string, action:string, duration:number){
    this.snackBar.open(msg, action, {
      duration:duration
    })
  }
  
  loadForm(servidor: Servidor) {
    this.form = this.formBuilder.group({
      matricula: [{value: servidor.matricula,  disabled: true}],
      cpf: [{value: servidor.pessoaFisica.cpf,  disabled: true}],
      sexo: [{value: servidor.pessoaFisica.genero, disabled: true}],
      nascimento: [{value: new Date(servidor.pessoaFisica.nascimento).toLocaleDateString(), disabled: true}],
      nome: [{value: servidor.pessoaFisica.nome, disabled: true}], 
      nomeMae: [{value: servidor.pessoaFisica.nomeMae, disabled: true}],
      nomePai: [{value: servidor.pessoaFisica.nomePai, disabled: true}],
    })
  }

}
