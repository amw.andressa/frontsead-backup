import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NomeacaoComponent } from './nomeacao.component';

describe('NomeacaoComponent', () => {
  let component: NomeacaoComponent;
  let fixture: ComponentFixture<NomeacaoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NomeacaoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NomeacaoComponent);

    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
