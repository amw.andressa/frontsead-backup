import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetailComponent } from './detail/detail.component';
import { FaltasComponent } from './faltas/faltas.component';

import { ListComponent } from './list/list.component';
import { NomeacaoComponent } from './nomeacao/nomeacao.component';
import { PesquisaComponent } from './pesquisa/pesquisa.component';

const routes: Routes = [
  {
    path: 'detalhar/:id', component: DetailComponent
  },
  {
    path: 'nomeacao/:id', component: NomeacaoComponent
  },

  {
    path: 'pesquisa', component: PesquisaComponent
  },
  {
    path: 'listar', component: ListComponent
  },
  {
    path: 'faltas', component: FaltasComponent
  },
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServidoresRoutingModule { }
