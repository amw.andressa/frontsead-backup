import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { EMPTY, Observable, of } from 'rxjs';

import { Servidor } from '../model/servidor';
import { ServidorService } from '../service/servidor/servidor.service';


@Component({
  selector: 'app-pesquisa',
  templateUrl: './pesquisa.component.html',
  styleUrls: ['./pesquisa.component.scss']
})
export class PesquisaComponent implements OnInit {

  form: FormGroup;
  servidores$: Observable<Servidor[]> = EMPTY;
  servidores_array!: Servidor[];
  mensagem = '';
  digite = '';
 

  displayedColumns = ['nome', 'cpf', 'matricula', 'acao']


  constructor(
    private formBuilder: FormBuilder,
    private servidorService: ServidorService,
    private router: Router,
  ) {
    this.form = this.formBuilder.group({
      matricula: [null],
      nome: [null],
      cpf: [null],
      selectPesquisa: ['1'],
    })
  }

  ngOnInit(): void {
  }

  onSearchMatricula() {

    let inputValue = this.form.value.matricula;
    if (inputValue && (inputValue = inputValue.trim() !== '')) {
      this.servidorService.getOneByMatricula(this.form.value.matricula).subscribe(
        {
          next:
            (p: any) => {
              if (p.content.length == 0) {
                const gridMensagem: HTMLElement | null = document.getElementById("t") as HTMLHeadingElement;
                gridMensagem.hidden = false;
                gridMensagem.textContent = this.mensagem
              } else {
                const gridMensagem: HTMLElement | null = document.getElementById("t") as HTMLHeadingElement;
                gridMensagem.hidden = true;
              }
              this.servidores_array = [],
                this.servidores_array = p.content,
                this.servidores$ = of(this.servidores_array);
            }
        }
      )
    }
    else {
      const gridMensagem: HTMLElement | null = document.getElementById("t") as HTMLHeadingElement;
      gridMensagem.hidden = false;
      gridMensagem.textContent = this.digite
    }
  }

  onSearchCpf() {

    let inputValue = this.form.value.matricula;
    if (inputValue && (inputValue = inputValue.trim() !== '')) {
      this.servidorService.getOneByCpfServidor(this.form.value.matricula).subscribe(
        {
          next:
            (p: any) => {
              if (p.content.length == 0) {
                const gridMensagem: HTMLElement | null = document.getElementById("t") as HTMLHeadingElement;
                gridMensagem.hidden = false;
                gridMensagem.textContent = this.mensagem
              } else {
                const gridMensagem: HTMLElement | null = document.getElementById("t") as HTMLHeadingElement;
                gridMensagem.hidden = true;
              }
              this.servidores_array = [],
                this.servidores_array = p.content,
                this.servidores$ = of(this.servidores_array);
            }
        }
      )
    }
    else {
      const gridMensagem: HTMLElement | null = document.getElementById("t") as HTMLHeadingElement;
      gridMensagem.hidden = false;
      gridMensagem.textContent = this.digite
    }
  }

  onSearchNome() {

    // let pesquisa =  this.servidorService.getByMatricula(this.form.value.matricula);
    let inputValue = this.form.value.matricula;
    if (inputValue && (inputValue = inputValue.trim() !== '')) {

      // let x = this.form.value.selectPesquisa;
      // if (x == 1) {
      //   pesquisa = this.servidorService.getOneByMatricula(this.form.value.matricula);
      // } 
      // if (x == 3) {
      // let pesquisa = ;
      // }if (x == 2) {
      //   pesquisa = this.servidorService.getByNameServidor(this.form.value.matricula);
      // }

      this.servidorService.getByNameServidor(this.form.value.matricula).subscribe(
        {
          next:
            (p: any) => {
              if (p.content.length == 0) {
                const gridMensagem: HTMLElement | null = document.getElementById("t") as HTMLHeadingElement;
                gridMensagem.hidden = false;
                gridMensagem.textContent = this.mensagem
              } else {
                const gridMensagem: HTMLElement | null = document.getElementById("t") as HTMLHeadingElement;
                gridMensagem.hidden = true;
              }
              this.servidores_array = [],
                this.servidores_array = p.content,
                this.servidores$ = of(this.servidores_array);
            }
        }
      )
    }
    else {
      const gridMensagem: HTMLElement | null = document.getElementById("t") as HTMLHeadingElement;
      gridMensagem.hidden = false;
      gridMensagem.textContent = this.digite
    }
  }

  //TODO: Na busca por nome ou cpf deve-se verificar se a pessoa já tem matrícula 

  tipoPesquisa() {
    let x = this.form.value.selectPesquisa;
    if (x == 1) {
      this.mensagem = 'Matrícula não encontrada'
      this.digite = 'Digite a matrícula'
      this.onSearchMatricula()

    } if (x == 2) {
      this.mensagem = 'Nome não encontrado'
      this.digite = 'Digite o nome'
      this.onSearchNome()
    }
    if (x == 3) {
      this.mensagem = 'CPF não encontrado'
      this.digite = 'Digite o CPF'
      this.onSearchCpf()
    }
  }

  enviarDetalhesServidor(id: number) {
    this.router.navigate(['servidores/detalhar/' + id.toString()], {})
  }
}