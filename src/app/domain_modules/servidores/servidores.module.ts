
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatAutocompleteModule } from '@angular/material/autocomplete';


import { MatTableModule } from '@angular/material/table';

import { DetailComponent } from './detail/detail.component';
import { NomeacaoComponent } from './nomeacao/nomeacao.component';
import { PesquisaComponent } from './pesquisa/pesquisa.component';
import { ServidoresRoutingModule } from './servidores-routing.module';
import { DialogMessageComponent } from './dialog-message/dialog-message.component';
import { ListComponent } from './list/list.component';
import { FaltasComponent } from './faltas/faltas.component';



@NgModule({
  declarations: [
    DetailComponent,

    NomeacaoComponent,
    PesquisaComponent,
    DialogMessageComponent,
    ListComponent,
    FaltasComponent
  ],
  imports: [
    CommonModule,
    ServidoresRoutingModule,
    MatInputModule,
    MatCardModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatDividerModule,
    MatIconModule,
    FlexLayoutModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule,
    MatSnackBarModule,
    MatButtonModule,

    MatAutocompleteModule,
    MatTableModule,
    MatRadioModule,
    MatDialogModule
  ]
})
export class ServidoresModule { }
