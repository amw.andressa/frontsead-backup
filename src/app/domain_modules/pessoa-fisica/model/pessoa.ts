export interface PessoaFisica {
    id: number,
    nome: string,
    email: string,
    genero: string,
    cpf: string,
    dataNascimento: string,
    tipoSanguineo: string, 
    nomeMae: string,
    nomePai: string,
    nomeSocial: string,
    estadoCivil: string,
    nacionalidade: string
}
