import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { catchError, Observable, of } from 'rxjs';
import { PessoaFisica } from '../model/pessoa';
import { PessoasService } from '../service/pessoas.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  pessoas_array: any;

  displayedColumns = ['nome', 'email', 'cpf', 'genero', 'dataNascimento', 'acao']

  constructor(
    private snackBar: MatSnackBar,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private pessoafisicaService: PessoasService
  ) {
    pessoafisicaService.listAll().subscribe(p => {this.pessoas_array = Object.values(p)[0], console.log(this.pessoas_array)})
   }

  ngOnInit(): void {
  }

  onError(msg: string, action: string, duration: number) {
    this.snackBar.open(msg, action, {
      duration: duration
    })
  }

  // Não está sendo usado
  // onAdd(){
  //   this.router.navigate(['servidores/cadastrar'], {relativeTo: this.activatedRoute})
  // }


  onDelete(id:number){

    this.pessoafisicaService.delete(id)
    .subscribe({
      next: (v) => console.log(v),
      error: (e) => console.log(e),
      complete: () => this.pessoas_array = this.pessoafisicaService.listAll() 
    }); 
  }

  testeId(id: number) {
    console.log(id)
  }

  onEdit(id: number) {
    this.router.navigate(['pessoas/editar/' + id.toString()], {})
  }

}
