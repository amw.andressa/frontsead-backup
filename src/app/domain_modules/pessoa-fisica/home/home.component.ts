import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Router, ActivatedRoute } from "@angular/router";
import { Observable, catchError, of } from "rxjs";
import { PessoaFisica } from "../model/pessoa";
import { PessoasService } from "../service/pessoas.service";

@Component({
    selector: 'app-homeComponent',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],

})

export class homeComponent implements OnInit {

    form: FormGroup;
    pessoas$: Observable<PessoaFisica[]>
    pessoas_array: PessoaFisica[] = [];



    displayedColumns = ['nome', 'email', 'cpf', 'genero', 'dataNascimento', 'acao']

    constructor(
        private snackBar: MatSnackBar,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private pessoafisicaService: PessoasService,
        private formBuilder: FormBuilder,
    ) {

        this.form = this.formBuilder.group({
            cpf: [null]
        })

        this.pessoas$ = pessoafisicaService.listAll()
            .pipe(
                catchError(error => {
                    console.log(error);
                    this.onError(error.message, "", 1000);
                    return of([])
                })
            )
            pessoafisicaService.listAll().subscribe(p => this.pessoas_array = p)
    }

    ngOnInit(): void {
    }
    onError(msg: string, action: string, duration: number) {
        this.snackBar.open(msg, action, {
            duration: duration
        })
    }

    onAdd() {
        this.router.navigate(['servidores/cadastrar'], { relativeTo: this.activatedRoute })
    }

    onDelete(id: number) {
        this.pessoafisicaService.delete(id)
            .subscribe({
                next: (v) => console.log(v),
                error: (e) => console.log(e),
                complete: () => this.pessoas$ = this.pessoafisicaService.listAll()
            });
    }

    testeId(id: number) {
        console.log(id)
    }

    onEdit(id: number) {
        this.router.navigate(['pessoas/editar/' + id.toString()], {})
    }

    pesqCpf() {
        console.log(this.form.value.cpf);
    }

}
