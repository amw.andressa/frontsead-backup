import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { PessoasService } from '../service/pessoas.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nomeacao',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {

  form: FormGroup

  constructor(
    private pessoafisicaService: PessoasService,

    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar,
    private location: Location,
    private router: Router
  ) {
    this.form = this.formBuilder.group({
      nome: [null],
      email: [null],
      genero: [null],
      cpf: [null],
      nascimento: [null],
      tipoSanguineo: [null],
      nomeMae: [null],
      nomePai: [null],
      estadoCivil: [null],
      nacionalidadePais: [null],
      naturalidadeCidade: [null],
      estadoVital: [null]


    })
  }
  ngOnInit(): void {
  }

  onSubmit() {
    
    // console.log('this.router.url.toString()', this.router.url.toString())
    if (this.router.url.toString() == '/pessoas/cadastrar') {

      console.log('insert do service', this.form.value.nascimento)
      this.pessoafisicaService.insert(this.form.value).subscribe({
        next: (v) => this.onSucess(),
        error: (e) => this.snackBar.open(e, "", { duration: 1000 }),
        complete: () => console.info('complete')
      }) 
    }
    else {
      console.log('Passou do if', this.router.url.toString())
    }
  }

  onSucess() {
    this.snackBar.open("Cadastro realizado com sucesso", "", { duration: 2000 })
    this.location.back();
  }
  
  onCancel() {
    this.location.back();
  }
}
