import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { GenericService } from '../../../services/generico-services';
import { PessoaFisica } from '../model/pessoa';


import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})



export class PessoasService extends GenericService<PessoaFisica> {


  constructor(protected http: HttpClient) { 
    super(http, environment.apiUrl + 'pessoasfisicas');
  }

}


 