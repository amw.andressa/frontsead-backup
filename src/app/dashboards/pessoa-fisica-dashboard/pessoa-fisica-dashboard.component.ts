import { Component, OnInit } from '@angular/core';
import { messages_dashboards } from '../messages-dashboards';

interface Cards {
  icon_card: string;
  titulo_card: string;
  descricao_card: string;
  btn_atalho_card : string;
  link_card: string;
}
@Component({
  selector: 'app-pessoa-fisica-dashboard',
  templateUrl: './pessoa-fisica-dashboard.component.html',
  styleUrls: ['./pessoa-fisica-dashboard.component.scss']
})
export class PessoaFisicaDashboardComponent implements OnInit {
  cardpessoa: Array<Cards> = [];
  mensagem = messages_dashboards;

  constructor() { }

  ngOnInit(): void {



    this.cardpessoa = [
      {
        icon_card: this.mensagem.card_pessoas_cadastrar_icon,
        titulo_card: this.mensagem.card_pessoas_cadastrar_titulo,
        descricao_card: this.mensagem.card_pessoas_cadastrar_descricao,
        link_card: this.mensagem.card_pessoas_cadastrar_link,
        btn_atalho_card: this.mensagem.card_pessoas_cadastrar_atalho,
      },
      {
        icon_card: this.mensagem.card_pessoas_listar_icon,
        titulo_card: this.mensagem.card_pessoas_listar_titulo,
        descricao_card: this.mensagem.card_pessoas_listar_descricao,
        link_card: this.mensagem.card_pessoas_listar_link,
        btn_atalho_card: this.mensagem.card_pessoas_listar_atalho,
      }
    ];
  }
}
