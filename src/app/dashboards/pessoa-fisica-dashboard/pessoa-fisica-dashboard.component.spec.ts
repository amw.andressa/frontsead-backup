import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PessoaFisicaDashboardComponent } from './pessoa-fisica-dashboard.component';

describe('PessoaFisicaDashboardComponent', () => {
  let component: PessoaFisicaDashboardComponent;
  let fixture: ComponentFixture<PessoaFisicaDashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PessoaFisicaDashboardComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PessoaFisicaDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
