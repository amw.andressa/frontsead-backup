import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainAdminDashboardComponent } from './main-admin-dashboard/main-admin-dashboard.component';
import { PessoaFisicaDashboardComponent } from './pessoa-fisica-dashboard/pessoa-fisica-dashboard.component';

import { MatCardModule } from '@angular/material/card'
import { DashboardsRoutingModule } from './dashboards-routing.module';

import {MatIconModule} from '@angular/material/icon'
import { FlexLayoutModule } from '@angular/flex-layout';

import { MatSidenavModule } from '@angular/material/sidenav';
import {MatBadgeModule} from '@angular/material/badge';
import { MatButtonModule } from '@angular/material/button';
import { ServidoresDashboardComponent } from './servidores-dashboard/servidores-dashboard.component';
import { MainComponentsModule } from '../main-components/main-components.module';

@NgModule({
  declarations: [
    MainAdminDashboardComponent,
    PessoaFisicaDashboardComponent,
    ServidoresDashboardComponent
  ],
  imports: [
    CommonModule,
    MatCardModule,
    MatIconModule,
    FlexLayoutModule,
    MatSidenavModule,
    
    DashboardsRoutingModule,
    MatBadgeModule,
    MatButtonModule,
    MainComponentsModule
    
  ]
})
export class DashboardsModule { }
