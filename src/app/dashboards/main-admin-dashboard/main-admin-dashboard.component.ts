import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router'
import { messages_dashboards } from '../messages-dashboards';

interface Cards {
  icon_card:       string;
  titulo_card:     string;
  descricao_card:  string;
  btn_atalho_card: string;
  link_card:       string;
}



@Component({
  selector: 'app-main-admin-dashboard',
  templateUrl: './main-admin-dashboard.component.html',
  styleUrls: ['./main-admin-dashboard.component.scss']
})
export class MainAdminDashboardComponent implements OnInit {
  
  
  mensagem = messages_dashboards;

  card: Array<Cards> = [];
  
  constructor(private router: Router) { 

    

  }

  

  ngOnInit(): void {
    

    document.addEventListener("keydown", e=> { 
      if(e.key === "1" && e.altKey){
        this.router.navigateByUrl('dashboard/pessoas');
      }else if(e.key === "2" && e.altKey){
        this.router.navigateByUrl('dashboard/servidores');
      } 
    });

  
    this.card = [
      {
        icon_card: this.mensagem.card_pessoas_icon,
        titulo_card: this.mensagem.card_pessoas_titulo,
        descricao_card: this.mensagem.card_pessoas_descricao,
        btn_atalho_card: this.mensagem.card_pessoas_atalho,
        link_card: this.mensagem.card_pessoas_link,       
      },
      {
        icon_card: this.mensagem.card_servidores_icon,
        titulo_card: this.mensagem.card_servidores_titulo,
        descricao_card: this.mensagem.card_servidores_descricao,
        btn_atalho_card: this.mensagem.card_servidores_atalho,
        link_card: this.mensagem.card_servidores_link,       
      },
      {
        icon_card:  this.mensagem.card_dependentes_icon,
        titulo_card: this.mensagem.card_dependentes_titulo,
        descricao_card: this.mensagem.card_dependentes_descricao,
        btn_atalho_card: this.mensagem.card_dependentes_atalho,
        link_card: this.mensagem.card_dependentes_link,      

      },
      {
        icon_card:  this.mensagem.card_representantes_icon,
        titulo_card: this.mensagem.card_representantes_titulo,
        descricao_card: this.mensagem.card_representantes_descricao,
        btn_atalho_card: this.mensagem.card_representantes_atalho,
        link_card: this.mensagem.card_representantes_link,

      },
     
    ];
  };
}
