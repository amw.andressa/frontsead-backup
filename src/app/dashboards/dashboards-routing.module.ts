import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {MainAdminDashboardComponent} from './main-admin-dashboard/main-admin-dashboard.component';
import { PessoaFisicaDashboardComponent } from './pessoa-fisica-dashboard/pessoa-fisica-dashboard.component';
import { ServidoresDashboardComponent } from './servidores-dashboard/servidores-dashboard.component';

export const routes: Routes = [
  {
    path: '', component: MainAdminDashboardComponent, data: {breadcrumb: '/Home'}
  },
  {
    path: 'pessoas', component: PessoaFisicaDashboardComponent, data: {breadcrumb: '/Pessoas'}
  },
  {
    path: 'servidores', component: ServidoresDashboardComponent, data: {breadcrumb: '/Servidor'}
  },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})


export class DashboardsRoutingModule { }
