import { Component, OnInit } from '@angular/core';
import { messages_dashboards } from '../messages-dashboards';

interface Cards {
  icon_card: string;
  titulo_card: string;
  descricao_card: string;
  btn_atalho_card : string;
  link_card: string;
}
@Component({
  selector: 'app-servidores-dashboard',
  templateUrl: './servidores-dashboard.component.html',
  styleUrls: ['./servidores-dashboard.component.scss']
})
export class ServidoresDashboardComponent implements OnInit {

  cards: Array<Cards> = [];
  menssagem = messages_dashboards;
  constructor() { }

  ngOnInit(): void {


    this.cards = [
      {
        icon_card: 'account_box',
        titulo_card: "NOMEAR SERVIDORES",
        descricao_card: "Nomeei pessoas para um cargo do Estado, incluindo-a na folha de pagamento.",
        btn_atalho_card: "Alt+1",
        link_card: "../../servidores/pesquisa",
      },
      {
        icon_card: 'account_box',
        titulo_card: "LISTAR SERVIDORES",

        descricao_card: "Liste todos os servidores vinculados a sua empresa.",
        btn_atalho_card: "Alt+2",
        link_card: "../../servidores/listar",
      },
      {
        icon_card: 'account_box',
        titulo_card: "REGISTRAR FALTAS",

        descricao_card: "Listagem e registro de faltas dos servidores.",
        btn_atalho_card: "Alt+3",
        link_card: "../../servidores/faltas",
      },
      
    ];
  }
}