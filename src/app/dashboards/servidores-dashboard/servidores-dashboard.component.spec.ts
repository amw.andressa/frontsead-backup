import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ServidoresDashboardComponent } from './servidores-dashboard.component';

describe('ServidoresDashboardComponent', () => {
  let component: ServidoresDashboardComponent;
  let fixture: ComponentFixture<ServidoresDashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ServidoresDashboardComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ServidoresDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
