export class messages_dashboards{
    
    /*card pessoas*/
    static card_pessoas_icon:string       = 'account_box';
    static card_pessoas_titulo:string     = 'PESSOAS FÍSICAS';
    static card_pessoas_descricao:string  = 'Cadastre, pesquise, delete e atribua perfil a pessoas físicas.';
    static card_pessoas_atalho:string     = 'Alt+1';
    static card_pessoas_link:string       = 'pessoas';


    /*card servidores*/
    static card_servidores_icon:string       = 'assignment_ind';
    static card_servidores_titulo:string     = 'SERVIDORES';
    static card_servidores_descricao:string  = 'Edite dados cadastrais, pesquise e nomeei servidores.';
    static card_servidores_atalho:string     = 'Alt+2';
    static card_servidores_link:string       = 'servidores';


    /*card dependentes*/
    static card_dependentes_icon:string       = 'groups';
    static card_dependentes_titulo:string     = 'DEPENDENTES';
    static card_dependentes_descricao:string  = 'Pesquise e insira dependentes de seus servidores.';
    static card_dependentes_atalho:string     = 'Alt+3';
    static card_dependentes_link:string       = '';

    /*card representantes legais*/
    static card_representantes_icon:string       = 'supervised_user_circle';
    static card_representantes_titulo:string     = 'REPRESENTANTES LEGAIS';
    static card_representantes_descricao:string  = 'Indique e pesquise representates legais para os dependentes vinculados a servidores.';
    static card_representantes_atalho:string     = 'Alt+4';
    static card_representantes_link:string       = '';


    //
    /**/
    static card_pessoas_cadastrar_icon:string       = 'assignment_ind';
    static card_pessoas_cadastrar_titulo:string     = 'CADASTRAR PESSOA FÍSICA';
    static card_pessoas_cadastrar_descricao:string  = 'Gerencie informações de servidores.';
    static card_pessoas_cadastrar_atalho:string     = 'Alt+1';
    static card_pessoas_cadastrar_link:string       = '../../pessoas/cadastrar';

    /**/
    static card_pessoas_listar_icon:string       = 'assignment_ind';
    static card_pessoas_listar_titulo:string     = 'LISTAR PESSOAS FÍSICAS';
    static card_pessoas_listar_descricao:string  = 'Gerencie informações de servidores.';
    static card_pessoas_listar_atalho:string     = 'Alt+2';
    static card_pessoas_listar_link:string       = '../../pessoas/listar';

   
    //
    /**/
    static card_servidores_cadastrar_icon:string       = 'account_box';
    static card_servidores_cadastrar_titulo:string     = 'NOMEAR SERVIDORES';
    static card_servidores_cadastrar_descricao:string  = 'Nomeei pessoas para um cargo do Estado, incluindo-a na folha de pagamento.';
    static card_servidores_cadastrar_atalho:string     = 'Alt+1';
    static card_servidores_cadastrar_link:string       = '../../servidores/pesquisa';

    /**/
    static card_servidores_listar_icon:string       = 'account_box';
    static card_servidores_listar_titulo:string     = 'LISTAR SERVIDORES';
    static card_servidores_listar_descricao:string  = 'Liste todos os servidores vinculados a sua empresa.';
    static card_servidores_listar_atalho:string     = 'Alt+2';
    static card_servidores_listar_link:string       = '../../servidores/nomeacao';


}