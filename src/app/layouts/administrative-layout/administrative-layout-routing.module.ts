import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


export const routes: Routes = [
  { 
    path: 'dashboard',
    loadChildren: () => import('../../dashboards/dashboards.module').then(m => m.DashboardsModule)
  },
  { 
    path: 'pessoas', 
    loadChildren: () => import('../../domain_modules/pessoa-fisica/pessoa-fisica.module').then(m => m.PessoaFisicaModule)
  },
  { 
    path: 'servidores', 
    loadChildren: () => import('../../domain_modules/servidores/servidores.module').then(m => m.ServidoresModule)
  }
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdministrativeLayoutRoutingModule { }

